FROM gitpod/workspace-full
USER root
RUN sudo apt-get update
RUN sudo apt-get purge cmake -y

RUN sudo apt-get install -y lcov

RUN sudo apt-get install -y graphviz

RUN sudo apt-get install -y \
    pandoc \
    texlive \
    texlive-xetex \
    texlive-latex-extra \
    texlive-lang-german \
    texlive-fonts-extra \
    gpp \
	ccache \
	cppcheck

RUN sudo apt-get autoremove -y
RUN sudo rm -rf /var/lib/apt/lists/*
