#!/usr/bin/env bash
#
#
set -euo pipefail
IFS="$(printf '\n\t')"

BUILD_DIR=build-coverage
mkdir -p "$BUILD_DIR"
cd "$BUILD_DIR"
cmake .. -DCMAKE_BUILD_TYPE=Debug -DCHECKER_ENABLE_UNITTEST=True -DCHECKER_ENABLE_COVERAGE=True -G "Sublime Text 2 - Unix Makefiles"
make -j && ./unittest
make coverage