#!/usr/bin/env bash
#
#
set -euo pipefail
IFS="$(printf '\n\t')"

BUILD_DIR=build-release

mkdir -p "$BUILD_DIR"
cd "$BUILD_DIR"
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
make -j
cpack