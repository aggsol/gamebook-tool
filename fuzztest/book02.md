## 1 {location=castle .fixed}

goto [2]\
goto [3]\
goto [4]

## 2 {location=kitchen}

goto [3]

## 3 {location=throne-room}

<#filler throne-king.png>\
<#centered Happy End!>

## 4 { location=castle combat=Manticore skill=10 stamina=12 }

<#combat Manticore|Skill 10 Stamina 12>

goto [6] \
goto [8]


## 6 {location=castle}

A

## 10 { location=secret }

goto [1]

## 8

Add <#item Head of the Manticore> to your inventory.

got [2]

goto [3]
