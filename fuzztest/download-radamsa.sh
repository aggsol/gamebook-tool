#!/usr/bin/env bash
#
set -euo pipefail
IFS="$(printf '\n\t')"

git clone https://gitlab.com/akihe/radamsa.git
cd radamsa
make -j
 
