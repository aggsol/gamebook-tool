#!/usr/bin/env bash
#
set -euo pipefail
IFS="$(printf '\n\t')"

checker=../build/gamebook-tool
cmd=radamsa/bin/radamsa

if ! command -v $cmd &> /dev/null
then
    echo "$cmd could not be found. Download radamsa first."
    exit
fi

if ! command -v $checker &> /dev/null
then
    echo "$checker could not be found. Build gamebook-tool first."
    exit
fi

intputfile=""
outputfile=""
counter=0
i=0

rm -f radamsain-* radamsaout-*

while true;
do
	inputfile="radamsain-${i}.md"
	outputfile="radamsaout-${i}.result"
	errorfile="radamsaout-${i}.error"

	#echo "${i}nputfile -> $outputfile"

	$cmd book01.md > "$inputfile"
	if ../build/gamebook-tool -d graph.dot --verbose --exit0 "$inputfile" >"$outputfile" 2>"$errorfile";
	then
		rm -f "$inputfile" "$outputfile" "$errorfile"
	else
		echo "Keep files for test ${i}"
		((counter += 1))
	fi
	if ((counter > 2)); then
		break
	fi

	((i += 1))
done

rm -f core.*

echo "$counter crashes"
