# gamebook-tool v0.2.0

**WORK IN PROGRESS**

gamebook-tool checks gamebooks written in Pandoc's Markdown for consistency and reachable sections.
It also parses a section's attributes like location, scene, items, combat and more.
Creates an optionally annotated dot graph for error checking and flow control.
It can mix/shuffle sections randomly and deterministic for release.

## Usage

```
$ gamebook-tool [Parameters] [Flags] [Input Files]
```

| Parameters                   |                                               |
| ---------------------------- | --------------------------------------------- |
|  -m, --mix [file]            | Mix sections, write to output [file]          |
|  -s, --seed [number]         | Mix random seed, default: 12345               |
|  -d, --dot-file [file]       | Create a dot file named [file]                |
|  -c, --color-seed [number]   | Color seed for the generated dot file         |

| Flags                        |                                               |
| ---------------------------- | --------------------------------------------- |
|  -h, --help                  | Show help text                                |
|  -l, --left-to-right         | Create left to right graph                    |
|  -a, --annotate-graph        | Annotate the graph nodes with section data    |
|  -n, --no-color              | Disable colored output                        |
|  -v, --verbose               | Show additional information                   |
|      --version               | Show version                                  |

## Supported Attributes

The following attributes are supported.
These can be used to annotate sections.
[For more details see Pandoc's Manual on heading identifiers.](https://pandoc.org/MANUAL.html#heading-identifiers)

| Attributes                   |                                               |
| ---------------------------- | --------------------------------------------- |
|  .todo                       | Section is unfinished and needs work          |
|  .fixed                      | Section will not be shuffled, keeps its number|
|  .secret                     | Section is secret, no direct reference        |
|  location=[value]            | Set the location or scene of a section        |
|  combat=[value]              | Section contains an enemy encounter           |

## Warnings

The gamebook-tool will emit various warnings

* Missing section numbers
* Unreachable sections, no other section has a refernce to it unless it is marked *secret*
* Todo sections, sections marked as *TODO*

## Visualisation

The gamebook-tool can create a dot file that can be visualised into a graphic (e.g. PNG) using [Graphviz](https://www.graphviz.org/).
Nodes are colored by the given location attributes.
Sections that are still *TODO* are filled red.
Ending sections/graph leaves are double outlined.
Fixed sections are boxes. 
Combat sections are diamonds.

### Example

```
$ gamebook-tool -d graph.dot --annotate-graph your-sections.md
$ dot -Tpng -o graph.png graph.dot
```

## Licenses

**gamebook-tool is GPLv3**

### Dependencies

* [rang is public domain (unlicense)](https://github.com/agauniyal/rang)
* [fmt is BSD licensed](https://github.com/fmtlib/fmt)
* [Pandoc Lua Filters are MIT licensed](https://github.com/pandoc/lua-filters)
* cliarguments-cpp is beerware
* tiny-unit is beerware

## Build


```
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE_DEBUG
cmake --build .
```