/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Analyser.hpp"
#include "Parser.hpp"
#include "Section.hpp"
#include <bits/stdint-uintn.h>
#include <cassert>
#include "fmt/format.h"
#include <stdexcept>

namespace bodhi
{
	Analyser::Analyser(Parser& p, bool verbose)
	: m_parser(p)
	, m_verbose(verbose)
	{

	}

	void Analyser::analyse()
	{
		const auto& sections = m_parser.getSections();

		if(sections.empty())
		{
			throw std::runtime_error("No sections to analyse");
		}

		uint32_t maxSecNum = sections.rbegin()->first;
		if(maxSecNum < m_parser.getMaxSectionNum())
		{
			maxSecNum = m_parser.getMaxSectionNum();
		}

		for(uint32_t i=1; i<=maxSecNum; ++i)
		{
			auto it = sections.find(i);
			if(it == sections.end())
			{
				m_missingSections.insert(i);
				continue;
			}

			assert(it->first == it->second.Number);

			if(it->second.LineNumber == 0)
			{
				assert(!it->second.Parents.empty());
				m_warnings.emplace_back(AnalyserWarning::Type::SectionNotFound,
					fmt::format("Section {0} not found. Referenced from section {1}",
						it->first, *it->second.Parents.cbegin()));
			}

			if(it->second.Parents.empty() && i != 1)
			{
				m_warnings.emplace_back(AnalyserWarning::Type::NoParents,
					fmt::format("Section {0} is unreachable", it->first));
			}

			auto secType = it->second.getType();
			assert(secType != Section::Type::Unset);
			if(secType == Section::Type::Todo)
			{
				m_warnings.emplace_back(AnalyserWarning::Type::Todo,
					fmt::format("Section {0} is unfinished/still a TODO", it->first));
			}

			if(it->second.isSecret() && !it->second.Parents.empty())
			{
				m_warnings.emplace_back(AnalyserWarning::Type::ReachableSecret,
					fmt::format("Section {0} is marked secret but reachable from section {1}",
					it->first, *it->second.Parents.cbegin()));
			}
		}

		createWarningsMissingSections();
	}

	void Analyser::createWarningsMissingSections()
	{
		uint32_t begin = 0;
		uint32_t end = 0;
		for(unsigned int m_missingSection : m_missingSections)
		{
			uint32_t curr = m_missingSection;
			if(begin == 0)
			{
				begin = end = curr;
			}
			else
			{
				if(m_missingSection == end+1)
				{
					end = curr;
				}
				else
				{
					if(begin == end)
					{
						m_warnings.emplace_back(AnalyserWarning::Type::MissingSections,
							fmt::format("Missing section {0}", begin));
					}
					else
					{
						m_warnings.emplace_back(AnalyserWarning::Type::MissingSections,
							fmt::format("Missing sections {0}-{1}", begin, end));
					}
					begin = end = curr;
				}
			}
		}

		if(begin != 0)
		{
			if(begin == end)
			{
				m_warnings.emplace_back(AnalyserWarning::Type::MissingSections,
					fmt::format("Missing section {0}", begin));
			}
			else
			{
				m_warnings.emplace_back(AnalyserWarning::Type::MissingSections,
					fmt::format("Missing sections {0}-{1}", begin, end));
			}
		}
	}
}