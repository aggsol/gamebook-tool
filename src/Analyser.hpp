/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <string>
#include <vector>
#include <set>

namespace bodhi
{
	class Parser;

	class AnalyserWarning
	{
	public:
		enum class Type
		{
			MissingSections,
			NoParents,
			Todo,
			ReachableSecret,
			SectionNotFound
		};

		AnalyserWarning(Type t, const std::string& txt)
		: WarningType(t)
		, Text(txt)
		{}

		Type 			WarningType;
		std::string		Text;
	};

	class Analyser
	{
	public:
		Analyser(Parser& parser, bool verbose);

		const std::vector<AnalyserWarning>&	getWarnings() const { return m_warnings; }

		void analyse();
		void createWarningsMissingSections();

	private:

		Parser&							m_parser;
		bool							m_verbose;
		std::vector<AnalyserWarning>	m_warnings;
		std::set<uint32_t>				m_missingSections;
	};
}