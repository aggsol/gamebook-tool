cmake_minimum_required(VERSION 3.14)

add_subdirectory(fmt)


# library
add_library(
    gamebook
STATIC
	Analyser.cpp
    DotExport.cpp
	Mixer.cpp
    Parser.cpp
	ParserException.cpp
    Section.cpp
	Writer.cpp
)

target_link_libraries(gamebook fmt::fmt)
target_compile_definitions(gamebook PRIVATE _FORTIFY_SOURCE=2)
target_compile_options(gamebook
    PRIVATE
    -Wall -Wextra -Wshadow -Wnon-virtual-dtor -Wold-style-cast
    -Woverloaded-virtual -Wzero-as-null-pointer-constant
    -pedantic -fPIE -fstack-protector-all -fno-rtti
)

if(CHECKER_ENABLE_COVERAGE)
if(CMAKE_BUILD_TYPE MATCHES Debug)
if(CMAKE_COMPILER_IS_GNUCXX)
	target_compile_options(gamebook
	    PRIVATE
		--coverage -fprofile-arcs -ftest-coverage
	)
endif()
endif()
endif()

# gamebook-tool
add_executable("gamebook-tool"
    main.cpp
)

target_link_libraries("gamebook-tool" gamebook)
target_compile_features("gamebook-tool" PRIVATE cxx_std_14)
target_compile_definitions("gamebook-tool" PRIVATE _FORTIFY_SOURCE=2)
target_compile_options("gamebook-tool"
	PRIVATE
    -Wall -Wextra -Wshadow -Wnon-virtual-dtor -Wold-style-cast -Woverloaded-virtual -Wzero-as-null-pointer-constant
    -pedantic -fPIE -fstack-protector-all -fno-rtti
)

# install target
include(GNUInstallDirs)
install(TARGETS "gamebook-tool"
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)