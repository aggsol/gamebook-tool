/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <fstream>
#include "fmt/format.h"
#include "DotExport.hpp"

namespace
{
    // Crc32.cpp
    // Copyright (c) 2011-2016 Stephan Brumme. All rights reserved.
    // see http://create.stephan-brumme.com/disclaimer.html
    //
    /// compute CRC32 (half-byte algorithm)
    uint32_t crc32(const void* data, size_t length, uint32_t previousCrc32)
    {
        uint32_t crc = ~previousCrc32; // same as previousCrc32 ^ 0xFFFFFFFF
        //   const uint8_t* current = (const uint8_t*) data;
        const uint8_t* current = static_cast<const uint8_t*>(data);

        /// look-up table for half-byte, same as crc32Lookup[0][16*i]
        static const uint32_t Crc32Lookup16[16] =
        {
        0x00000000,0x1DB71064,0x3B6E20C8,0x26D930AC,0x76DC4190,0x6B6B51F4,0x4DB26158,0x5005713C,
        0xEDB88320,0xF00F9344,0xD6D6A3E8,0xCB61B38C,0x9B64C2B0,0x86D3D2D4,0xA00AE278,0xBDBDF21C
        };

        while (length-- != 0)
        {
        crc = Crc32Lookup16[(crc ^  *current      ) & 0x0F] ^ (crc >> 4);
        crc = Crc32Lookup16[(crc ^ (*current >> 4)) & 0x0F] ^ (crc >> 4);
        current++;
        }

        return ~crc; // same as crc ^ 0xFFFFFFFF
    }

    std::string colorToHex(uint32_t value)
    {
        const uint8_t r = ((value) + 255)/2;
        const uint8_t g = ((value >> 8) + 255)/2;
        const uint8_t b = ((value >> 16) + 255)/2;

        return fmt::format("{0:x}{1:x}{2:x}", r, g, b);
    }

    const std::string ATTRIB_LOCATION = "location";
}

namespace bodhi
{
    DotExport::DotExport(const std::string& filename, uint32_t colorSeed,
        bool lrGraph, bool showData)
    : m_filename(filename)
    , m_colorSeed(colorSeed)
    , m_lrGraph(lrGraph)
    , m_showData(showData)
    {
        if(filename.empty())
        {
            throw std::runtime_error("Missing dot filename");
        }
    }

    void DotExport::exportDotFile(const std::map<uint32_t, Section>& sections)
    {
        std::ofstream dotfile(m_filename);
        if(not dotfile.is_open())
        {
            throw std::runtime_error("Cannot write dot file");
        }
        dotfile << "digraph G {\n";

        if(m_lrGraph)
        {
            dotfile << "    rankdir=LR;\n";
        }

        dotfile << "    forcelabels=true;\n"
        << "    graph [fontname = \"Arial\"];\n"
        << "    edge [fontname = \"Arial\"];\n"
        << "    node [fontname = \"Arial\" style=filled fillcolor=white];\n";

        for(const auto& s: sections)
        {
            if(s.first == 0) // skip anything before section 1
            {
                continue;
            }

            for(auto& c: s.second.Children)
            {
                dotfile << "    " << s.first << " -> " << c << ";\n";
            }

            dotfile << "    " << s.first << " [fillcolor=\"#";
            if(s.second.getType() == Section::Type::Todo)
            {
                dotfile << "ff0000\"]";
            }
            else
            {
                dotfile << getColor(s.second.tryKey(ATTRIB_LOCATION)) << "\"]";
            }

            if(s.second.getType() == Section::Type::Combat)
            {
                dotfile << " [shape=diamond]";
            }
            else if(s.second.getType() == Section::Type::Fixed)
            {
                dotfile << " [shape=box]";
            }

            if(s.second.Children.empty())
            {
                dotfile << " [peripheries=2]";
            }

            if(m_showData)
            {
                dotfile << " [label=\"" << s.first << "\\n";
                for(auto& d: s.second.Data)
                {
                    if(d.first == ATTRIB_LOCATION) continue;
                    dotfile << d.first << "=" << d.second << "\\n";
                }
                for(auto& a: s.second.Attributes)
                {
                    dotfile << a << "\\n";
                }
                dotfile << "\"]";
            }
            dotfile << ";\n";
        }

        if(m_showData)
        {
            // begin subgraph
            dotfile << "\n    subgraph legend {\n";
            dotfile << "        { rank=same ";
            for(auto& entry: m_colorMap)
            {
                dotfile << "\"" << entry.first << "\" ";
            }
            dotfile << "}\n";

            for(auto& entry: m_colorMap)
            {
                dotfile << "        \"" << entry.first
                    << "\" [shape=record label=\"" << entry.first
                    << "\" fillcolor=\"#" << entry.second;
                dotfile << "\"";
                dotfile << "]\n";
            }

            dotfile << "    }\n";
            // end subgraph
        }

        dotfile << "}\n";
    }

    std::string DotExport::getColor(const std::string& location)
    {
        if(location.empty()) return "ffffff";

        auto it = m_colorMap.find(location);
        if(it == m_colorMap.end())
        {
            const uint32_t value = crc32(location.c_str(), location.size(), m_colorSeed);
            auto hexColor = colorToHex(value);
            m_colorMap[location] = hexColor;
            return hexColor;
        }

        return it->second;
    }
}