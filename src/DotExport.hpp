/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "Section.hpp"

#include <string>
#include <map>

namespace bodhi
{
    class DotExport
    {
    public:
        DotExport(const std::string& filename, uint32_t colorSeed,
            bool lrGraph, bool showData);
		DotExport() = delete;
		DotExport(const DotExport&) = delete;

        void exportDotFile(const std::map<uint32_t, Section>& sections);

    private:
        std::string getColor(const std::string& location);

        std::string                         m_filename;
        uint32_t                            m_colorSeed;
        bool                                m_lrGraph;
        bool                                m_showData;
        std::map<std::string, std::string>  m_colorMap;

    };
}