/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Mixer.hpp"
#include "Section.hpp"
#include "StringReplace.hpp"

#include <bits/stdint-uintn.h>
#include <cassert>
#include <deque>
#include <functional>
#include <iostream>
#include <random>
#include "fmt/format.h"
#include "rang.hpp"

namespace
{
    /*
    * Returns an int from range [min, max]
    */
    template<class URNG>
    inline int randint(URNG& generator, int min, int max)
    {
        std::uniform_int_distribution<int> distribution(min, max);
        return distribution(generator);
    }
}

namespace bodhi
{
	Mixer::Mixer(uint32_t seed, bool verbose)
	: m_seed(seed)
	, m_verbose(verbose)
	{

	}

	void Mixer::mix(std::map<uint32_t, Section>& sections)
	{
		m_mapping.clear();
		createMapping(sections);

		std::map<uint32_t, bodhi::Section> mixedSections;
		std::map<std::string, std::string> placeholders;

		for(auto& s: sections)
		{
			const uint32_t mixed = m_mapping.at(s.first);
			s.second.Number = mixed;

			// First Pass
			for(auto& child: s.second.Children)
			{
				const uint32_t mappedChild = m_mapping.at(child);

				std::string before = fmt::format("[{0}]", child);
				std::string placeholder = fmt::format("[temp-{0}-{1}]", child, mappedChild);
				std::string after = fmt::format("[{0}]", mappedChild);

				placeholders[placeholder] = after;

				if(m_verbose)
                {
                    std::cerr << "before='" << before << "'\t "
                    << "temp  ='" << placeholder << "'\t "
                    << "after ='" << after << "'\n";
                }

				for(auto& line: s.second.Lines)
				{
					line = bodhi::replaceSubStr(line, before, placeholder);
				}
			}

			// Second pass
			for(auto& line: s.second.Lines)
			{
				for(auto& p: placeholders)
				{
					line = bodhi::replaceSubStr(line, p.first, p.second);
				}
			}

			assert(mixed == s.second.Number);
			mixedSections[mixed] = s.second;
		}
		sections = mixedSections;
	}

	void Mixer::createMapping(const std::map<uint32_t, Section>& sections)
	{

		std::deque<uint32_t> srcNums;

		for(const auto& s: sections)
		{
			if(s.first == 0) continue;
			//std::cerr << "s.first=" << s.first << " s.second.Number=" << s.second.Number << "\n";
			assert(s.first == s.second.Number);
			if(s.second.isFixed())
			{
				m_mapping[s.first] = s.first;
			}
			else
			{
				srcNums.push_back(s.first);
			}
		}

		std::default_random_engine generator(m_seed);

        const int minDelta = std::max<int>(3, srcNums.size()/10);
        const int maxAttempts = std::max<int>(10, srcNums.size()/4);

		if(m_verbose)
		{
			std::cerr << "Mixer: minDelta=" << minDelta
				<< " maxAttempts=" << maxAttempts
				<< " sections.size()=" << sections.size()
				<< "\n";
		}

		while(srcNums.size() > 1)
		{
			assert(!srcNums.empty());
			int index = randint(generator, 0, srcNums.size()-1);

			const int a = srcNums.at(index);
			srcNums.erase(srcNums.begin() + index);

			assert(!srcNums.empty());
			index = randint(generator, 0, srcNums.size()-1);

			int b = srcNums.at(index);

			int counter = 0;
            while( (abs(a-b) < minDelta) && (counter < maxAttempts))
            {
                index = randint(generator, 0, srcNums.size()-1);
                b = srcNums.at(index);
                counter++;
            }
            if(counter >= maxAttempts)
            {
                std::cerr << rang::fg::yellow
                << "Warning: Could not map section " << a << " farther away\n"
                << rang::fg::reset;
            }

            srcNums.erase(srcNums.begin() + index);

            assert(a != b);
            assert(m_mapping.find(a) == m_mapping.end());
            assert(m_mapping.find(b) == m_mapping.end());

            m_mapping[a] = b;
            m_mapping[b] = a;
		}

		assert(srcNums.size() <= 1);
		if(srcNums.size() == 1)
		{
			const int c = srcNums.at(0);
			m_mapping[c] = c;
			std::cerr << rang::fg::yellow
                << "Warning: Section " << c << " cannot be mixed\n"
                << rang::fg::reset;
		}

		if(m_verbose)
		{
			for(auto m: m_mapping)
			{
				std::cerr << m.first << " -> " << m.second << "\n";
			}
		}

		assert(m_mapping.size() == sections.size());
	}
}