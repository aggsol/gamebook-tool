/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "Section.hpp"

#include <bits/stdint-uintn.h>
#include <cstdint>
#include <map>


namespace bodhi
{
	class Mixer
	{
	public:
		Mixer(uint32_t seed, bool verbose);
		Mixer() = delete;
		Mixer(const Mixer& other) = delete;

		void mix(std::map<uint32_t, Section>&);
		const std::map<uint32_t, uint32_t>& getMapping() const { return m_mapping; }

	private:

		void createMapping(const std::map<uint32_t, Section>&);

		uint32_t						m_seed;
		bool							m_verbose;
		std::map<uint32_t, uint32_t>	m_mapping;
	};
}