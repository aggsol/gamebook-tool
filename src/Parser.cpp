/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Parser.hpp"
#include "ParserException.hpp"
#include <algorithm>
#include <bits/stdint-uintn.h>
#include <cctype>
#include "fmt/format.h"
#include <cassert>
#include <stdexcept>
#include <fstream>

namespace bodhi
{
    Parser::Parser(bool verbose)
    : m_verbose(verbose)
    {

    }

	uint32_t Parser::parseSections(const std::string& path)
	{
		std::ifstream file;
		file.open(path);
		if(not file.is_open())
		{
			throw std::runtime_error(fmt::format("Cannot open file: {0}", path));
		}
		return parseSections(file);
	}

    uint32_t Parser::parseSections(std::istream& stream)
    {
		m_parsingRun++;
        std::string line;
        m_lineCounter = 0;
        while(stream)
        {
            std::getline(stream, line);
            m_lineCounter++;
            parseLine(line);
        }

		if(m_verbose)
		{
			std::cout << "Parsed lines=" << m_lineCounter
				<< " parsed sections=" << m_sections.size()
				<< " maxSecNum=" << m_maxSecNum
				<< "\n";
		}
		return m_lineCounter;
    }

    void Parser::parseLine(const std::string& line)
    {
        if(line.empty())
        {
			if( m_currSection > 0)
			{
            	getCurrentSection().Lines.push_back(line);
			}
            return;
        }

		m_state = State::Start;
		bool addLine = true;
        for(unsigned i=0; i<line.size() && m_state != State::End; ++i)
        {
            const bool isAtEnd = (i == (line.size() - 1));
            const char c = line.at(i);

            char peek = 0;
            if(not isAtEnd)
            {
                peek = line.at(i+1);
            }

            //std::cout << fmt::format("Line:{0:<3}:{3:<3}: c={1:<1} peek={2:<1} State={4:<12} isAtEnd={5} parsedVal={6}\n",
                //m_lineCounter, c, peek, i, getStateName(m_state), isAtEnd, m_parsedValue);

            switch(m_state)
            {
            case State::Start:
                if(c == '#' && peek == '#') // ATX Header
                {
                    m_parsedValue.clear();
                    m_state = State::AtxHeading;
                }
                else if(c == '#')
                {
                     throw ParserException("Invalid heading. Only level 2 ATX headings are allowed.", m_lineCounter, i);
                }
                else if(c == '<' && peek == '#') // Macro
                {
                    m_state = State::Macro;
                }
                else if(isAtEnd)
                {
                    m_state = State::End;
                }
                else
                {
                    m_state = State::InParagraph;
                }
            break;

            case State::Macro:
                if(c == '#' && std::isspace(peek))
                {
                    throw ParserException("Invalid macro format", m_lineCounter, i);
                }
                else if(c == '>')
                {
					if(isAtEnd)
					{
                    	m_state = State::End;
					}
					else
					{
						m_state = State::InParagraph;
					}
                }
                else if(isAtEnd)
                {
                    throw ParserException("Missing macro closing '>'");
                }
            break;

            case State::AtxHeading:
				addLine = false;
                if(peek == '#')
                {
                    throw ParserException("Invalid heading. Section headings must be level 2 '##'", m_lineCounter, i);
                }
				
				if(std::isdigit(c))
                {
                    m_parsedValue += c;
                }
				
				if(c == '{' || isAtEnd)
                {
					if(m_parsedValue.empty())
					{
						throw ParserException("Invalid heading. Must be numeric.", m_lineCounter, i);
					}

					setCurrentSectionNum(m_parsedValue);

					if(getCurrentSection().ParsingRun > 0 && getCurrentSection().ParsingRun != m_parsingRun)
					{
						throw ParserException(fmt::format("Section {0} already parsed before", m_parsedValue)
						, m_lineCounter, i);
					}
					getCurrentSection().ParsingRun = m_parsingRun;
                    getCurrentSection().LineNumber = m_lineCounter;
					getCurrentSection().Number = m_currSection;
					m_parsedValue.clear();

					if(isAtEnd)
                	{
                    	m_state = State::End;
                	}
					else
					{
                    	m_state = State::Attributes;
					}
                }
				
				if(c == '-' || c == '\t')
				{
					throw ParserException(fmt::format("Invalid character '{0}'", c)
							, m_lineCounter, i);
				}

            break;

			case State::Attributes:
                if( c == '#')
                {
                    throw ParserException("Invalid character '#'. Header identifiers are not allowed.");
                }
                else if(c == '.')
                {
                    m_state = State::Class;
                }
			    else if(c == '=')
                {
                    m_tmpVal = m_parsedValue;
                    m_parsedValue.clear();
                    m_state = State::KeyValue;
                }
			    else if(std::isalnum(c) || c == '-' || c == '_')
				{
					m_parsedValue += c;
				}
                else if(c == '}')
                {
                    m_state = State::End;
                }
                else if(c != '}' && isAtEnd)
                {
				    throw ParserException("Missing closing '}'",
                        m_lineCounter, i);
                }
                else if(not std::isspace(c))
                {
                    throw ParserException(fmt::format("Invalid character '{2}' in attribute", c),
                        m_lineCounter, i);
                }
			break;

            case State::Class:
                if(std::isalnum(c) || c == '-' || c == '_')
                {
                    m_parsedValue += c;
                }
                else if(std::isspace(c))
                {
                    if(not m_parsedValue.empty())
                    {
                        getCurrentSection().Attributes.push_back(m_parsedValue);
                        m_parsedValue.clear();
                    }
                    m_state = State::Attributes;
                }
                else if(c == '}')
                {
                    if(not m_parsedValue.empty())
                    {
                        getCurrentSection().Attributes.push_back(m_parsedValue);
                        m_parsedValue.clear();
                    }
                    m_state = State::End;
                }
                else
                {
                    throw ParserException(fmt::format("Invalid character '{2}' in class", c),
                        m_lineCounter, i);
                }
            break;

			case State::KeyValue:
                if(std::isalnum(c) || c == '-' || c == '_')
                {
                    m_parsedValue += c;
                }
                else if(std::isspace(c) || c == '}')
                {
                    if(m_tmpVal.empty())
                    {
                        throw ParserException("Invalid key value pair", m_lineCounter, i);
                    }
                    if(m_parsedValue.empty())
                    {
                        throw ParserException("Invalid value", m_lineCounter, i);
                    }

					std::transform(m_tmpVal.begin(), m_tmpVal.end(), m_tmpVal.begin(), [](unsigned char letter){ return std::tolower(letter); });

                    getCurrentSection().Data[m_tmpVal] = m_parsedValue;
                    m_parsedValue.clear();
                    if(isAtEnd)
                    {
                        m_state = State::End;
                    }
                    else
                    {
                        m_state = State::Attributes;
                    }
                }
                else {
                    throw ParserException(fmt::format("Line {0}:{1}: Invalid character '{2}' in value",
                             m_lineCounter, i, c));
                }
			break;

            case State::InParagraph:
                if(isAtEnd)
                {
                    m_state = State::End;
                }
                else if(c == '[')
                {
                    m_state = State::Reference;
                }
				else if(c == '<')
				{
					if(peek == '#')
					{
						m_state = State::Macro;
					}
					else
					{
						throw ParserException("Invalid macro in paragraph", m_lineCounter, i);
					}
				}
            break;

            case State::Reference:
                if(std::isdigit(c))
                {
                    m_parsedValue += c;
                }
                else if(c == ']')
                {
                    if(m_parsedValue.empty())
					{
						throw ParserException(fmt::format("Line {0}:{1}: Missing section number",
                            m_lineCounter, i));
					}
                    
					uint32_t child = static_cast<uint32_t>(std::stoi(m_parsedValue));
                    if(child == 0 || child > 999)
                    {
                        throw ParserException(fmt::format("Line {0}:{1}: Invalid section number={2}",
                            m_lineCounter, i, m_parsedValue));
                    }

					if(child == m_currSection)
					{
						throw ParserException(fmt::format("Line {0}:{1}: Section {2} cannot reference itself",
						m_lineCounter, i, child));
					}

                    getCurrentSection().Children.insert(child);
                    m_sections[child].Number = child;
					m_sections[child].Parents.insert(m_currSection);

                    m_parsedValue.clear();
                    if(isAtEnd)
                    {
                        m_state = State::End;
                    }
                    else
                    {
                        m_state = State::InParagraph;
                    }
                }
                else
                {
                    throw ParserException(fmt::format("Line {0}:{1}: Invalid character={2} in section reference",
                        m_lineCounter, i, c));
                }
            break;

            default:
                throw ParserException(fmt::format("Line {2}:{3}: Unhandled character='{0}' Parser state={1}",
                    c, getStateName(m_state), m_lineCounter, i));
            }
        }

        if(m_state != State::End)
        {
            throw ParserException(fmt::format("Line {1}: Invalid parser state {0}",
                    getStateName(m_state), m_lineCounter));
        }

		if(addLine)
		{
        	getCurrentSection().Lines.push_back(line);
		}
    }

	void Parser::setCurrentSectionNum(const std::string& val)
	{
		assert(not val.empty());
		m_currSection = 0;
		m_currSection = std::stoi(val);

		if(m_currSection == 0)
		{
			throw ParserException(fmt::format("Line {0}: Section 0 is not allowed", m_lineCounter));
		}

		if(m_currSection > 999)
		{
			throw ParserException(fmt::format("Line {0}: Only up to 999 sections are allowed", m_lineCounter));
		}

		if(m_currSection > m_maxSecNum)
		{
			m_maxSecNum = m_currSection;
		}
		m_sections[m_currSection].Number = m_currSection;
	}

    const char *Parser::getStateName(Parser::State state)
	{
        switch(state)
        {
            case State::Attributes:
                return "Attributes";
            case State::InParagraph:
                return "InParagraph";
            case State::AtxHeading:
                return "AtxHeading";
            case State::KeyValue:
                return "KeyValue";
            case State::End:
                return "End";
            case State::Start:
                return "Start";
            case State::Reference:
                return "Reference";
            case State::Class:
                return "Class";
            case State::Macro:
                return "Macro";
        }

        return "Unknown";
    }

	void Parser::evaluateSections()
	{
		for(auto& s: m_sections)
		{
			s.second.evaluate();
		}
	}
}
