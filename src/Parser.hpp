/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <bits/stdint-uintn.h>
#include <cstdint>
#include <iostream>
#include <map>

#include "Section.hpp"

namespace bodhi
{
    class Parser
    {
    public:
        explicit Parser(bool verbose = false);
		Parser(const Parser& other) = default;

        uint32_t parseSections(const std::string& path);
		uint32_t parseSections(std::istream& stream);
		void evaluateSections();

        const std::map<uint32_t, Section>& getSections() const { return m_sections; }

		uint32_t getMaxSectionNum() const { return m_maxSecNum; }

    private:
        enum class State
        {
            Start = 0,
            InParagraph = 1,
            AtxHeading = 2,
            Attributes = 4,
            KeyValue = 8,
            Reference = 16,
            Class = 32,
            Macro = 64,
            End = -1
        };

        void parseLine(const std::string& line);

        Section& getCurrentSection() { return m_sections.at(m_currSection); }

        bool toInt(const std::string& input, int& outValue);

        static const char* getStateName(State state);

		void setCurrentSectionNum(const std::string& val);

        uint32_t                    m_lineCounter = 0;
        uint32_t                    m_currSection = 0;
        std::map<uint32_t, Section> m_sections;
        State 						m_state = State::Start;
        std::string 				m_parsedValue;
        std::string                 m_tmpVal;

		bool						m_verbose = false;
		uint32_t					m_maxSecNum = 0;
		int32_t						m_parsingRun = 0;
    };
}
