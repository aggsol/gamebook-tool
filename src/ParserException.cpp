/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ParserException.hpp"

#include "fmt/format.h"

namespace bodhi
{
    ParserException::ParserException(const std::string& what, uint32_t line,
        uint32_t col)
    : std::runtime_error(fmt::format(":{0}:{1}: error: {2}",
        line, col, what))
    , m_line(line)
    , m_column(col)
    {

    }
}