/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <stdexcept>

namespace bodhi
{
	class ParserException : public std::runtime_error
	{
	public:
		explicit ParserException( const std::string& what)
		: std::runtime_error(what)
		{}

        ParserException(const std::string& what, uint32_t line, uint32_t col);

        uint32_t Line() const { return m_line; }
        uint32_t Column() const { return m_column; }

    private:
        uint32_t        m_line = 0;
        uint32_t        m_column = 0;
	};
}