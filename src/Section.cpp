/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Section.hpp"

namespace bodhi
{

    std::string Section::tryKey(const std::string& key) const
    {
        auto it = Data.find(key);
        if(it == Data.end())
        {
            return "";
        }
        return it->second;
    }

    void Section::evaluate()
    {
        if(m_type == Type::Unset)
        {
            m_type = Type::None;

            for(const auto& a: Attributes)
            {
                if(a == "todo")
                {
                    setType(Type::Todo);
                }
                else if(a == "combat")
                {
                    setType(Type::Combat);
                }
                else if(a == "fixed")
                {
                    setType(Type::Fixed);
					m_isFixed = true;
                }
				else if( a == "secret")
				{
					// TODO: Are there sections that are secret but not fixed?
					m_isSecret = true;
					m_isFixed = true;
					setType(Type::Fixed);
				}
            }

			for(const auto& kv: Data)
			{
				if(kv.first == "combat")
				{
					setType(Type::Combat);
				}
			}
        }
    }

    void Section::setType(Section::Type t)
    {
        if(t > m_type)
        {
            m_type = t;
        }
    }
}