/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BODHI_SECTION_HPP
#define BODHI_SECTION_HPP

#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <vector>

namespace bodhi
{
class Section
{
public:

	Section() = default;
	Section(const Section& other) = default;

    // Tricky: Mind the order! Higher values have priority
    enum class Type: std::uint32_t
    {
        Unset = 0,
        None = 1,
        Combat = 2,
        Fixed = 4,
        Todo = 8
    };

    uint32_t                    			Number = 0;

    std::vector<std::string>    			Lines;
	std::vector<std::string>				Attributes;
	std::map<std::string, std::string>		Data;
    std::set<uint32_t>          			Parents;
    std::set<uint32_t>          			Children;
	uint32_t								LineNumber = 0;
	int32_t									ParsingRun = -1;

	void			evaluate();
    Type            getType() const { return m_type; }
	bool			isSecret() const { return m_isSecret; }
	bool			isFixed() const { return m_isFixed; }
    std::string     tryKey(const std::string& key) const;

private:
    void            setType(Type t);

	Type            m_type = Type::Unset;
	bool			m_isSecret = false;
	bool			m_isFixed = false;
};
}

#endif