#pragma once

#include <string>

namespace bodhi
{
	/*
    * Taken from: https://stackoverflow.com/questions/4643512/replace-substring-with-another-substring-c
    */
    inline std::string replaceSubStr(std::string& str, const std::string& target,
    const std::string& sub)
    {
        if(target == sub || target.empty())
        {
            return str;
        }

        size_t index = 0;
        while (true)
        {
             /* Locate the substring to replace. */
             index = str.find(target, index);
             if (index == std::string::npos)
             {
                 break;
             }

             /* Make the replacement. */
             str.replace(index, target.size(), sub);

             /* Advance index forward so the next iteration doesn't pick it up as well. */
             index += sub.size();
        }
        return str;
    }
}