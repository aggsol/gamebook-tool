/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Writer.hpp"
#include <bits/stdint-uintn.h>
#include "fmt/format.h"
#include <iostream>

namespace bodhi
{
	Writer::Writer(bool verbose)
	: m_verbose(verbose)
	{

	}

	void Writer::writeSections(const std::map<uint32_t, bodhi::Section>& sections,
		std::ostream& strm) const
	{
		uint32_t line = 1;
		for(auto s: sections)
		{
			if(m_verbose)
			{
				strm << fmt::format("{0:<4}:", line);
			}

			strm << "## " << s.first;
			if(s.second.Attributes.size() > 0 || s.second.Data.size() > 0)
			{
				strm << " {";

				for(auto& a: s.second.Attributes)
				{
					strm << " ." << a;
				}

				for(auto& d: s.second.Data)
				{
					strm << " " << d.first << "=" << d.second;
				}

				strm << " }\n";
			}
			else
			{
				strm << "\n";
			}
			line++;

			for(auto l: s.second.Lines)
			{
				if(m_verbose)
				{
					strm << fmt::format("{0:<4}:", line);
				}

				strm << l << "\n";
				line++;
			}
		}
	}
}