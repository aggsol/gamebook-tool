/*
 Copyright (C) 2019  Kim HOANG <foss@aggsol.de>
 This file is part of gamebook-tool.

 gamebook-tool is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Analyser.hpp"
#include "DotExport.hpp"
#include "Mixer.hpp"
#include "Parser.hpp"
#include "ParserException.hpp"
#include "Version.hpp"
#include "Writer.hpp"
#include <cassert>
#include "CliArguments.hpp"
#include "fmt/format.h"
#include <fstream>
#include <iostream>
#include "rang.hpp"

namespace
{
    void printUsage()
    {
        std::cout
		<< "gamebook-tool v" << VERSION_MAJOR << "." << VERSION_MINOR << "." << VERSION_PATCH << "\n"
        << "Usage: gamebook-tool [Parameters] [Flags] [Input Files]\n"
		<< "\n"
        << "Check gamebook sections in Pandoc Markdown\n"
        << "Example: gamebook-tool sections.md\n"
        << "\n"
        << "Parameters:\n"
		<< "  -m, --mix <file>            Mix sections, write output into file\n"
        << "  -s, --seed <number>         Mix seed, default: 12345\n"
        << "  -d, --dot-file <file>       Create a dot file named <file>\n"
        << "  -c, --color-seed <number>   Color seed for the generated dot file\n"
        << "\n"
        << "Flags:\n"
        << "  -h, --help                  Show this help text\n"
        << "  -l, --left-to-right         Create left to right graph\n"
        << "  -a, --annotate-graph        Annotate the graph nodes with section data\n"
        << "  -n, --no-color              Disable colored output, same as NO_COLOR\n"
		<< "      --exit0                 Always return exit code 0\n"
        << "  -v, --verbose               Show additional information\n"
        << "      --version               Show version\n"
		<< "\n"
		<< "Report bugs at https://gitlab.com/aggsol/gamebook-tool/issues\n"
		<< "gamebook-tool Copyright (C) 2019  Kim HOANG\n"
    	<< "This program comes with ABSOLUTELY NO WARRANTY.\n"
    	<< "This is free software, and you are welcome to redistribute it\n"
    	<< "under certain conditions.\n"
        << std::endl;
    }
}

int main(int argc, char* argv[])
{
    bodhi::CliArguments cli(argc, argv);
    if(cli.getOpt("", "version"))
    {
        std:: cout << fmt::format("v{0}.{1}.{2}", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH)
		<< std::endl;
        return 0;
    }

    if(cli.getOpt("h", "help"))
    {
        printUsage();
        return 0;
    }

    const std::string NONE = "<none>";

    auto seed = cli.getOpt<unsigned>("s", "seed", 12345);
    auto mix = cli.getOpt<std::string>("m", "mix", NONE);
    auto dot = cli.getOpt<std::string>("d", "dot-file", NONE);
    auto verbose = cli.getOpt("v", "verbose");
    auto noColor = cli.getOpt("n", "no-color");
    auto colorSeed = cli.getOpt<uint32_t>("c", "color-seed", 0);
    auto lrGraph = cli.getOpt("l", "left-to-right");
    auto annotate = cli.getOpt("a", "annotate-graph");
	auto exitZero = cli.getOpt("", "exit0");

	auto envNoColor = getenv("NO_COLOR");
	if(noColor || envNoColor != nullptr)
    {
        rang::setControlMode(rang::control::Off);
    }

	if(cli.arguments().size() == 0)
	{
		std::cerr << rang::fg::red << "Error: No input files"
            << rang::fg::reset << "\n" << "Try --help for details\n";
        
		if(exitZero) return 0;
		return 103;
	}

	if(mix != NONE && dot != NONE)
	{
		std::cerr << rang::fg::red
            << "Error: Cannot combine section mixing and dot file creation."
            << rang::fg::reset << "\n";
		
		if(exitZero) return 0;
		return 104;
	}

    try
    {
		bodhi::Parser parser(verbose);
		for(const auto& filename: cli.arguments())
		{
			try
			{
				std::ifstream file;
				file.open(filename);
				if(not file.is_open())
				{
					std::cerr << rang::fg::red
						<< "Error: Cannot open input file: " << filename
						<< rang::fg::reset << "\n"
						<< "Try --help for help or --verbose for more information\n";
					
					if(exitZero) return 0;
					return 101;
				}
				if(verbose)
				{
					std::cout << "Parsing file: " <<  filename << "\n";
				}

				parser.parseSections(file);
			}
			catch(const bodhi::ParserException& pex)
			{
				std::cerr << rang::fg::red
				<< filename
				<< pex.what()
				<< rang::fg::reset << "\n"
				<< "Try --help for help or --verbose for more information\n";
				
				if(exitZero) return 0;
				return 104;
			}
		}
		parser.evaluateSections();

		std::map<uint32_t, bodhi::Section> sections = parser.getSections();
		if(mix != NONE)
		{
			bodhi::Mixer mixer(seed, verbose);
			mixer.mix(sections);

			bodhi::Writer writer(verbose);
			if(mix == "stdout")
			{
				writer.writeSections(sections, std::cout);
			}
			else
			{
				std::ofstream mixFile(mix);
                if(not mixFile.is_open())
                {
                    throw std::runtime_error(fmt::format("Cannot create file {0}.", mix));
                }
				writer.writeSections(sections, mixFile);
			}
		}

		if(verbose)
		{
			bodhi::Writer writer(verbose);
			writer.writeSections(sections, std::cout);
		}

		bodhi::Analyser analyser(parser, verbose);
		analyser.analyse();

		const auto& warnings = analyser.getWarnings();
		for(const auto& w: warnings)
		{
			std::cerr << rang::fg::yellow
				<< "Warning: " << w.Text
				<< rang::fg::reset << "\n";
		}

		if(dot != NONE)
        {
            bodhi::DotExport dotExport(dot, colorSeed, lrGraph, annotate);
            dotExport.exportDotFile(sections);
        }
		else
		{
			if(annotate)
			{
				std::cerr << rang::fg::yellow
					<< "Warning: Annotate graph flag is ignored"
					<< rang::fg::reset << "\n";
			}
			if(lrGraph)
			{
				std::cerr << rang::fg::yellow
					<< "Warning: Left to right graph flag is ignored"
					<< rang::fg::reset << "\n";
			}
		}
    }
    catch(std::exception& ex)
    {
        std::cerr << rang::fg::red
            << "Error: " << ex.what()
            << rang::fg::reset << "\n"
			<< "Try --help for help or --verbose for more information\n";
        
		if(exitZero) return 0;
		return 102;
    }
    return 0;
}
