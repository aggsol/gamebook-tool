#include "tiny-unit.hpp"

#include <string>
#include <sstream>

#include "../src/Analyser.hpp"
#include "../src/Parser.hpp"

class AnalyserTest : public tiny::Unit
{
public:
    AnalyserTest()
    : tiny::Unit(__FILE__)
    {
		tiny::Unit::registerTest(&AnalyserTest::sectionNotFound, "sectionNotFound");
		tiny::Unit::registerTest(&AnalyserTest::reachableSecret, "reachableSecret");
		tiny::Unit::registerTest(&AnalyserTest::todoSections, "todoSections");
        tiny::Unit::registerTest(&AnalyserTest::missingSections, "missingSections");
    }

	static void sectionNotFound()
	{
		const std::string input =
R"(## 1

This is section 1.
Goto [2]

)";
		std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        parser.parseSections(stream);
		parser.evaluateSections();

		auto sections = parser.getSections();

		bodhi::Analyser analyser(parser, false);
		analyser.analyse();

		const auto& warnings = analyser.getWarnings();

		TINY_ASSERT_EQUAL(warnings.size(), 1);

		TINY_ASSERT_OK(warnings[0].WarningType == bodhi::AnalyserWarning::Type::SectionNotFound);
	}

	static void reachableSecret()
	{
		const std::string input =
R"(## 1

This is section 1.
Goto [2]

## 2 { .secret }
This is the end
)";
		std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        parser.parseSections(stream);
		parser.evaluateSections();

		bodhi::Analyser analyser(parser, false);
		analyser.analyse();

		const auto& warnings = analyser.getWarnings();

		TINY_ASSERT_EQUAL(warnings.size(), 1);

		TINY_ASSERT_OK(warnings[0].WarningType == bodhi::AnalyserWarning::Type::ReachableSecret);
	}

	static void todoSections()
	{
		const std::string input = R"(## 1 {.todo} )";

		std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        parser.parseSections(stream);
		parser.evaluateSections();
		TINY_ASSERT_EQUAL(parser.getMaxSectionNum(), 1);

		const auto& sections = parser.getSections();
		TINY_ASSERT_EQUAL(sections.size(), 1);

		bodhi::Analyser analyser(parser, false);
		analyser.analyse();

		const auto& warnings = analyser.getWarnings();

		TINY_ASSERT_EQUAL(warnings.size(), 1);

		TINY_ASSERT_OK(warnings[0].WarningType == bodhi::AnalyserWarning::Type::Todo);
	}

	static void missingSections()
	{
		const std::string input =
R"(## 1

This is section 1.
Goto [4]

## 4
This is the end
)";

        std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        parser.parseSections(stream);
		parser.evaluateSections();
		TINY_ASSERT_EQUAL(parser.getMaxSectionNum(), 4);

		const auto& sections = parser.getSections();
		TINY_ASSERT_EQUAL(sections.size(), 2);

		bodhi::Analyser analyser(parser, false);
		analyser.analyse();

		const auto& warnings = analyser.getWarnings();

		TINY_ASSERT_EQUAL(warnings.size(), 1);
	}
};

AnalyserTest analyserTest;