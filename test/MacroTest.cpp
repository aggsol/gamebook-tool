#include "tiny-unit.hpp"

#include <string>
#include <sstream>

#include "../src/Parser.hpp"
#include "../src/ParserException.hpp"

class MacroTest : public tiny::Unit
{
public:
    MacroTest()
    : tiny::Unit(__FILE__)
    {
		tiny::Unit::registerTest(&MacroTest::invalidInParagraph, "invalidInParagraph");
		tiny::Unit::registerTest(&MacroTest::macroInParagraph, "macroInParagraph");
		tiny::Unit::registerTest(&MacroTest::invalidMacro, "invalidMacro");
        tiny::Unit::registerTest(&MacroTest::skipMacro, "skipMacro");
    }

	static void invalidInParagraph()
	{
		std::string input =
R"(## 1

aaa bbb < #bad ccc> ddd eee

)";
		std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        TINY_ASSERT_TRY()
            parser.parseSections(stream);
        TINY_ASSERT_CATCH(bodhi::ParserException);
	}


	static void macroInParagraph()
	{
		std::string input =
R"(## 1

foobar

## 2

Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor

<#combat Ogre|Skill=8 Stamina=8>

Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt

 )";

        std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        parser.parseSections(stream);

        const auto& sections = parser.getSections();
        TINY_ASSERT_EQUAL(sections.size(), 2);
	}

    static void invalidMacro()
    {
        std::string input =
R"(## 1

This is section 1.
<# macro foobar>
)";
        std::stringstream stream;
        stream << input;
        bodhi::Parser parser;

        TINY_ASSERT_TRY()
            parser.parseSections(stream);
        TINY_ASSERT_CATCH(bodhi::ParserException);
    }

    static void skipMacro()
    {
        std::string input =
R"(## 1

This is section 1.
<#macro foobar> Goto [2]
)";
        std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        parser.parseSections(stream);

        const auto& sections = parser.getSections();
        TINY_ASSERT_EQUAL(sections.size(), 2);
    }
};

MacroTest macroTest;
