#include "tiny-unit.hpp"

#include <string>
#include <sstream>

#include "../src/Section.hpp"
#include "../src/Mixer.hpp"

class MixerTest : public tiny::Unit
{
public:
    MixerTest()
    : tiny::Unit(__FILE__)
    {
		 tiny::Unit::registerTest(&MixerTest::miniMix, "miniMix");
	}

	static void miniMix()
	{
		std::map<uint32_t, bodhi::Section> sections;

		bodhi::Section sec1;
		sec1.Number = 1;
		sections[1] = sec1;

		bodhi::Section sec4;
		sec4.Number = 4;
		sections[4] = sec4;

		bodhi::Section sec10;
		sec10.Number = 10;
		sections[10] = sec10;

		sections[20].Number = 20;

		bodhi::Mixer mixer(1234, false);
		mixer.mix(sections);

		TINY_ASSERT_EQUAL(mixer.getMapping().size(), 4);
	}
};

MixerTest mixerTest;