#include "tiny-unit.hpp"

#include <string>
#include <sstream>

#include "../src/Parser.hpp"
#include "../src/ParserException.hpp"

class MultiFileTest : public tiny::Unit
{
public:
    MultiFileTest()
    : tiny::Unit(__FILE__)
    {
		tiny::Unit::registerTest(&MultiFileTest::loadTwo, "loadTwo");
		tiny::Unit::registerTest(&MultiFileTest::duplicateSections, "duplicateSections");
    }

	static void loadTwo()
	{
		bodhi::Parser parser(false);
		parser.parseSections("../test/linear01.md");
		TINY_ASSERT_EQUAL(parser.getMaxSectionNum(), 3);

		TINY_ASSERT_EQUAL(parser.getSections().size(), 4);

		parser.parseSections("../test/linear02.md");
		TINY_ASSERT_EQUAL(parser.getMaxSectionNum(), 6);

		TINY_ASSERT_EQUAL(parser.getSections().size(), 6);
	}

	static void duplicateSections()
	{
		bodhi::Parser parser(false);
		parser.parseSections("../test/linear01.md");
		TINY_ASSERT_TRY()
			parser.parseSections("../test/linear01.md");
		TINY_ASSERT_CATCH(bodhi::ParserException);
	}
};

MultiFileTest multifileTest;
