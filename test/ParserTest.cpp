#include "tiny-unit.hpp"

#include <string>
#include <sstream>

#include "../src/Parser.hpp"
#include "../src/ParserException.hpp"

class ParserTest : public tiny::Unit
{
public:
    ParserTest()
    : tiny::Unit(__FILE__)
    {
		tiny::Unit::registerTest(&ParserTest::badSectionNum, "badSectionNum");
        tiny::Unit::registerTest(&ParserTest::selfRef, "selfRef");
        tiny::Unit::registerTest(&ParserTest::parseReferences, "parseReferences");
        tiny::Unit::registerTest(&ParserTest::parseAttributes, "parseAttributes");
        tiny::Unit::registerTest(&ParserTest::parseHeading, "parseHeading");
    }

	static void badSectionNum()
	{
        std::string input =
R"(## -1562443517 {location=kitchen .todo}

**TODO**

What happens when the player pushes the red button?
Need to write more sections about the kitchen.
)";
		std::stringstream stream;
        stream << input;
        bodhi::Parser parser;

		TINY_ASSERT_TRY()
			parser.parseSections(stream);
		TINY_ASSERT_CATCH(bodhi::ParserException);
	}

	static void selfRef()
	{
        std::string input =
R"(## 1

This is section 1.
Goto [2]

## 2 { location=foobar }
}
This is section 2. Goto [2] or die here!
)";
		std::stringstream stream;
        stream << input;
        bodhi::Parser parser;

		TINY_ASSERT_TRY()
			parser.parseSections(stream);
		TINY_ASSERT_CATCH(bodhi::ParserException);
	}

    static void parseReferences()
    {
        std::string input =
R"(## 1

This is section 1.
Goto [2]\
Got0 [3]

## 2

This is section 2. Goto [4] or die here!

## 3
This section 3.
Goto [4]

## 4
This is the end
)";

        std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        uint32_t numLines = parser.parseSections(stream);

		const auto& sections = parser.getSections();
		TINY_ASSERT_EQUAL(sections.size(), 4);

		TINY_ASSERT_EQUAL(parser.getMaxSectionNum(), 4);

		const auto& sec1 = sections.at(1);
		TINY_ASSERT_EQUAL(sec1.Attributes.size(), 0);
        TINY_ASSERT_EQUAL(sec1.Data.size(), 0);
        TINY_ASSERT_EQUAL(sec1.Children.size(), 2);
        TINY_ASSERT_EQUAL(sec1.Parents.size(), 0);

        const auto& sec2 = sections.at(2);
        TINY_ASSERT_EQUAL(sec2.Attributes.size(), 0);
        TINY_ASSERT_EQUAL(sec2.Data.size(), 0);
        TINY_ASSERT_EQUAL(sec2.Children.size(), 1);
        TINY_ASSERT_EQUAL(sec2.Parents.size(), 1);

        const auto& sec3 = sections.at(3);
        TINY_ASSERT_EQUAL(sec3.Attributes.size(), 0);
        TINY_ASSERT_EQUAL(sec3.Data.size(), 0);
        TINY_ASSERT_EQUAL(sec3.Children.size(), 1);
        TINY_ASSERT_EQUAL(sec3.Parents.size(), 1);

        const auto& sec4 = sections.at(4);
        TINY_ASSERT_EQUAL(sec4.Attributes.size(), 0);
        TINY_ASSERT_EQUAL(sec4.Data.size(), 0);
        TINY_ASSERT_EQUAL(sec4.Children.size(), 0);
        TINY_ASSERT_EQUAL(sec4.Parents.size(), 2);
    }

    static void parseAttributes()
    {
        std::string input =
R"(
## 1 {location=home .todo daytime=night .fixed }

)";

		std::stringstream stream;
        stream << input;
        bodhi::Parser parser(false);
        uint32_t numLines = parser.parseSections(stream);

		TINY_ASSERT_EQUAL(parser.getMaxSectionNum(), 1);

        TINY_ASSERT_EQUAL(parser.getSections().size(), 1);
        const bodhi::Section& section = parser.getSections().at(1);
        TINY_ASSERT_EQUAL(section.Attributes.size(), 2);
        TINY_ASSERT_EQUAL(section.Attributes.at(0), "todo");

        TINY_ASSERT_EQUAL(section.Data.size(), 2);
        TINY_ASSERT_EQUAL(section.Data.at("location"), "home");
    }

    static void parseHeading()
    {
        std::string input =
R"(## 1

## 2

##    3

##   4    {}
)";

        std::stringstream stream;
        stream << input;
        bodhi::Parser parser;
        uint32_t numLines = parser.parseSections(stream);

		TINY_ASSERT_EQUAL(parser.getMaxSectionNum(), 4);
		TINY_ASSERT_EQUAL(numLines, 8);
		TINY_ASSERT_EQUAL(parser.getSections().size(), 4);
    }
};

ParserTest parserTest;
