#include "tiny-unit.hpp"

#include <string>
#include <sstream>

#include "../src/StringReplace.hpp"

class StringReplaceTest : public tiny::Unit
{
public:
    StringReplaceTest()
    : tiny::Unit(__FILE__)
    {
		tiny::Unit::registerTest(&StringReplaceTest::basic, "basic");
    }

	static void basic()
	{
		std::string input;
		input = "aaaaaaa";
		TINY_ASSERT_EQUAL(bodhi::replaceSubStr(input, "aa", "bbb"), "bbbbbbbbba");

		input = "2b nd4 ,f4";
		TINY_ASSERT_EQUAL(bodhi::replaceSubStr(input, "2B", "ND4"), "2b nd4 ,f4");

		input = "foo bar foobar";
		TINY_ASSERT_EQUAL(bodhi::replaceSubStr(input, "bar", "foo"), "foo foo foofoo");
	}
};

StringReplaceTest stringReplaceTest;
