## 1 {location=castle}

The castle is well lit and cozy warm.
Thick carpets damp your every step.
The floors are barren and there is nobody here!

Do you want to go to the kitchen, turn to [2]

Do you want to enter the throne room, turn to [3]

Do you want to yell into the hallways, turn to [4]

## 2 {location=kitchen}

The cook is fled in ahurry and there is no food left.
You rummage through the pots and dirty dished.
There is no food to befound.
You are still very hungry.
On the other side of the entrance you find a red button on the wall.

Do you want to enter the throne room, turn to [3]\
Do you want to press the red button, turn to [5]

## 3 {location=throne-room}

Carefully do you enter the huge throne room.
Candles are lit everywhere and the smell of grilled chicken fills the room.
You sit on the throne.
You are now the king!

<#filler throne-king.png>\
<#centered Happy End!>

## 4 { location=castle combat=Manticore skill=10 stamina=12 }

There is no echo in the castle.
The torches in the hallway flicker.
Your yelling attracted a huge manticore.

“Why are you yelling in my castle?” the manticore asks with a deep voice.

It is time to fight the manticore!

<#combat Manticore|Skill 10 Stamina 12>

Do you posses the <#item Sword of Aptitude>, turn to [6]

If you defeat the manitcore, turn to [8]

Otherwise your bloody corpse is eaten by manticore for dinner.
Your life and adventure end here.

## 5 {location=kitchen .todo}

**TODO**
What happens when the player pushes the red button?
Need to write more sections about the kitchen.

## 6 {location=castle}

You are a lying cheater!
There is no <#item Sword of Aptitude>.
The manticore bites your yummy head off.
Your life, adventure and lies end here in a painful bloody mess.

## 10 { location=secret }

This is a secret section.
You are not supposed to be here.

Return to the castle entrance, turn to [1]

## 8

You defeated the monstrous beast!
The manticore lays before you drawing its last breath.
You hack of its head to keep it as a trophy.
Add <#item Head of the Manticore> to your inventory.

Do you want to go to the kitchen, turn to [2]

Do you want to enter the throne room, turn to [3]
